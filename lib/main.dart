import 'package:desafio_toro/theme.dart';
import 'package:flutter/material.dart';
import 'package:desafio_toro/screens/app.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bolsa',
      theme: CustomColors.myTheme,
      home: App(),
    );
  }
}
