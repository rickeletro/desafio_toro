class Quote {
  String _symbol;
  double _value;
  double _timestamp;

  Quote({String symbol, double value, double timestamp}) {
    this._symbol = symbol;
    this._value = value;
    this._timestamp = timestamp;
  }

  String get symbol => _symbol;
  double get value => _value;
  DateTime get timestamp => DateTime.fromMillisecondsSinceEpoch(_timestamp.round() * 1000);

  Quote.fromJson(Map<String, dynamic> json) {
    _symbol = json.keys.first;
    _value = json.values.first;
    _timestamp = json['timestamp'];
  }
}