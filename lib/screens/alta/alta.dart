import 'dart:convert';

import 'package:desafio_toro/screens/shared/listItem.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

class Alta extends StatefulWidget {
  Alta({Key key}) : super(key: key);
  final channel = IOWebSocketChannel.connect("ws://localhost:8080/quotes");
  @override
  _AltaState createState() => _AltaState();
}

class _AltaState extends State<Alta> {
 var paperMap = {};
  final _scrollController = ScrollController();
  

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Scrollbar(
        child: SingleChildScrollView(
          controller: _scrollController,
          padding: EdgeInsets.all(16),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                StreamBuilder(
                  stream: widget.channel.stream,
                  builder: (context, snapshot) {
                    var data = snapshot.data;
                    Map dataDecoded = jsonDecode(data);
                    var paper = dataDecoded.keys.first;
                    var price = dataDecoded.values.first;

                    paperMap.addAll({paper: price});
                    return Scrollbar(
                      child: ListView.separated(
                        physics: AlwaysScrollableScrollPhysics(),
                        separatorBuilder: (context, index) {
                          return Padding(padding: EdgeInsets.all(6.0));
                        },
                        shrinkWrap: true,
                        itemCount: paperMap.length,
                        itemBuilder: (context, index) => ListItem(
                          {paperMap.keys.toList()[index]: paperMap.values.toList()[index]},
                          onTap: () {
                            print(context);
                          },
                        ),
                      ),
                    );
                  },
                ),
              ]),
        ),
      ),
    );
  }
}
