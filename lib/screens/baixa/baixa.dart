import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:desafio_toro/screens/shared/listItem.dart';
import 'package:web_socket_channel/io.dart';

class Baixa extends StatefulWidget {
  Baixa();

  @override
  _BaixaState createState() => _BaixaState();
}

class _BaixaState extends State<Baixa> {
  var paperMap = {};
  final _scrollController = ScrollController();
  final channel = IOWebSocketChannel.connect("ws://localhost:8080/quotes");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Scrollbar(
        child: SingleChildScrollView(
          controller: _scrollController,
          padding: EdgeInsets.all(16),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                StreamBuilder(
                  stream: channel.stream,
                  builder: (context, snapshot) {
                    var data = snapshot.data;
                    Map dataDecoded = jsonDecode(data);
                    var paper = dataDecoded.keys.first;
                    var price = dataDecoded.values.first;

                    paperMap.addAll({paper: price});
                    return Scrollbar(
                      child: ListView.separated(
                        physics: AlwaysScrollableScrollPhysics(),
                        separatorBuilder: (context, index) {
                          return Padding(padding: EdgeInsets.all(6.0));
                        },
                        shrinkWrap: true,
                        itemCount: paperMap.length,
                        itemBuilder: (context, index) => ListItem(
                          {paperMap.keys.toList()[index]: paperMap.values.toList()[index]},
                          onTap: () {
                            print("Press buton");
                          },
                        ),
                      ),
                    );
                  },
                ),
              ]),
        ),
      ),
    );
  }
}
