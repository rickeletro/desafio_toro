import 'package:desafio_toro/screens/baixa/baixa.dart';
import 'package:desafio_toro/screens/carteira/carteira.dart';
import 'package:desafio_toro/screens/home/home.dart';
import 'package:desafio_toro/screens/alta/alta.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  String _title;
  int _bottomSelectedIndex = 0;
 

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  Widget buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        setState(() {
          _bottomSelectedIndex = index;
        });
      },
      children: <Widget>[
        Home(),
        Alta(),
        Baixa(),
        Carteira(),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _title = "";
  }

  void pageChanged(int index) {
    setState(() {
      _bottomSelectedIndex = index;
      // pageController.animateToPage(index, duration: Duration(milliseconds: 200), curve: Curves.ease);
      pageController.jumpToPage(index);
      switch (index) {
        case 0:
          {
            _title = "";
          }
          break;
        case 1:
          {
            _title = 'Alta';
          }
          break;
        case 2:
          {
            _title = 'Baixa';
          }
          break;
        case 3:
          {
            _title = 'Carteira';
          }
          break;
      }
    });
  }

  List<BottomNavigationBarItem> _buildNavigationItems() {
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          MdiIcons.homeAnalytics,
        ),
        title: Text('Resumo'),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MdiIcons.trendingUp,
        ),
        title: Text('Alta'),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MdiIcons.trendingDown,
        ),
        title: Text('Baixa'),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MdiIcons.wallet,
        ),
        title: Text('Carteira'),
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: Padding(
          padding: EdgeInsets.only(left: 8.0, right: 3.0, top: 25.0, bottom: 0.0),
          child: AppBar(
              title: Text(
                _title,
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
              ),
              elevation: 0.0),
        ),
      ),
//      drawer: Drawer(),
      body: buildPageView(), //CARREGA CONTEUDO
      bottomNavigationBar: Container(
        height: 70,
        alignment: Alignment.topCenter,
        decoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
        ),
        padding: EdgeInsets.only(top: 5),
        child: BottomNavigationBar(
          elevation: 0,
          fixedColor: Theme.of(context).cursorColor,
          unselectedItemColor: Colors.grey.shade400,
          onTap: (index) {
            pageChanged(index);
          },
          currentIndex: _bottomSelectedIndex,
          items: _buildNavigationItems(),
          type: BottomNavigationBarType.fixed,
          iconSize: 26,
          selectedFontSize: 8,
          unselectedFontSize: 8,
          selectedLabelStyle: TextStyle(height: 1.8),
          unselectedLabelStyle: TextStyle(height: 1.8),
        ),
      ),
    );
  }
}
