import 'dart:convert';

import 'package:desafio_toro/screens/shared/listItem.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

class Home extends StatefulWidget {
  Home();

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _scrollController = ScrollController();
  final channel = IOWebSocketChannel.connect("ws://localhost:8080/quotes");
  var paperMap = {};

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Scrollbar(
        child: SingleChildScrollView(
          controller: _scrollController,
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Olá, Ricardo!",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xffffffff)),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "Seu Patrimônio é : 15.000,00",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xffffffff)),
              ),
              SizedBox(
                height: 25,
              ),
              StreamBuilder(
                stream: channel.stream,
                builder: (context, snapshot) {
                  var data = snapshot.data;
                  Map dataDecoded = jsonDecode(data);
                  var paper = dataDecoded.keys.first;
                  var price = dataDecoded.values.first;
                  
                  paperMap.addAll({paper: price});
                  return ListView.separated(
                    physics: AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (context, index) {
                      return Padding(padding: EdgeInsets.all(6.0));
                    },
                    shrinkWrap: true,
                    itemCount: paperMap.length,
                    itemBuilder: (context, index) => ListItem(
                      {paperMap.keys.toList()[index]: paperMap.values.toList()[index]},
                      onTap: () {
                        print(paperMap.keys.toList()[index]);
                      },
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
