import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';

class ListItem extends StatefulWidget {
  final Map data;
  final Function onTap;
  var dataXy = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];
  ListItem(this.data, {Key key, this.onTap}) : super(key: key);

  @override
  _ListItemState createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  @override
  Widget build(BuildContext context) {
    // print(widget.data);
    var subTitleFontSize = Theme.of(context).textTheme.subtitle.fontSize;
    return Material(
      color: Color(0xFFf5f5f5),
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: InkWell(
        onTap: widget.onTap,
        child: Container(
          padding: EdgeInsets.all(12.0),
          width: MediaQuery.of(context).size.width,
          //height: 116,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              left: 3.0, right: 6.0, top: 1.0, bottom: 3.0),
                          margin: EdgeInsets.only(right: 18.0),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6)),
                              color: Theme.of(context).textSelectionColor),
                          child: Icon(
                            MdiIcons.trendingUp,
                            color: Theme.of(context).primaryColor,
                            size: 16.0,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 18.0),
                          child: Text(
                            "-1.7%",
                            style: TextStyle(
                                color: Theme.of(context).errorColor,
                                fontSize: subTitleFontSize,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 18.0),
                          child: Text(
                            widget.data.keys.first != null
                                ? '${widget.data.keys.first}'
                                : '',
                            style: TextStyle(
                                color: Theme.of(context).hintColor,
                                fontSize: 18.0,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 21.0),
                          child: Text(
                            '${widget.data.values.first ?? widget.data.values.first ?? ''}',
                            style: TextStyle(
                              color: Theme.of(context).hintColor,
                              fontSize: subTitleFontSize,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          width: 76.0,
                          height: 42.0,
                          child: new Sparkline(
                            data: widget.dataXy,
                            lineWidth: 3.0,
                            lineColor: Colors.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
