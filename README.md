# desafio_toro

Este é um app desenvolvido em Flutter. 

# Iniciando

Para rodar este app em seu ambiente basta seguir:

Faça o clone:
git clone https://rickeletro@bitbucket.org/rickeletro/desafio_toro.git

Você pode instalar pacotes a partir da linha de comando:
$ flutter pub get

Para executar o app no VSCODE:
Windows:
CRTL + F5
MacOS:
ˆcontrol + F5

## Obs.


Este projeto Foi desenvolvido em Flutter.

Alguns recursos para você começar se este é seu primeiro projeto Flutter:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
